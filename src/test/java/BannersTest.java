import org.junit.Assert;
import org.junit.Test;

public class BannersTest {

    private final Banners banners = new BannersPseudoRandoms();

    @Test
    public void shouldReturnEmptyArrayWhenInitialIsEmpty() {
        Assert.assertEquals(0, banners.getBanners(new int[0], 100000).length);
    }

    @Test
    public void shouldReturnEmptyWhenAskedToShow0() {
        Assert.assertEquals(0, banners.getBanners(new int[]{1, 2, 3, 4, 5}, 0).length);

    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenBannersAmountLessThenAskedToShow() {
        Assert.assertEquals(0, banners.getBanners(new int[]{1, 2, 3, 4, 5}, 6).length);
    }


    @Test
    public void shouldShowBannersWithSameProbability() {
        int[] wasShownTimes = new int[5];
        for (int i = 0; i < 10000; i++) {
            for (int banner : banners.getBanners(new int[]{4, 0, 1, 3, 2}, 3)) {
                wasShownTimes[banner]++;
            }
        }
        printArr(wasShownTimes);
    }

    public static void printArr(int[] arr) {
        StringBuilder result = new StringBuilder("Arr: ");
        for (int a : arr) {
            result.append(a).append(", ");
        }
        System.out.println(result);
    }


}