import java.util.Random;

public class BannersPseudoRandoms implements Banners {

    public int[] getBanners(int[] banners, int amountToShow) {
        if (banners.length == 0 || amountToShow == 0) {
            return new int[0];
        }

        if (amountToShow > banners.length) {
            throw new IllegalArgumentException(String.format("Cannot show more banners: [%s] than was passed: [%s]", amountToShow, banners.length));
        }

        int[] result = new int[amountToShow];

        fillRandomPseudoIndices(result, banners.length);

        restoreRealIndices(result);

        fillWithBannerValues(banners, result);

        return result;
    }

    private void fillRandomPseudoIndices(int[] bannersToShow, int totalBannersAmount) {
        Random random = new Random();
        for (int i = 0; i < bannersToShow.length; i++) {
            bannersToShow[i] = random.nextInt(totalBannersAmount);
            totalBannersAmount--;
        }
    }

    private void restoreRealIndices(int[] bannersToShow) {
        for (int i = 0; i < bannersToShow.length; i++) {
            for (int j = i + 1; j < bannersToShow.length; j++) {
                if (bannersToShow[i] <= bannersToShow[j]) {
                    bannersToShow[j]++;
                }
            }
        }
    }

    private void fillWithBannerValues(int[] banners, int[] bannersToShow) {
        for (int i = 0; i < bannersToShow.length; i++) {
            bannersToShow[i] = banners[bannersToShow[i]];
        }
    }
}
